/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package orders;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author Rienk
 */
public class Order {
    private String datum;
    private Klant klant;
    private ArrayList<Integer> producten;

    public Order(String datum, Klant klant, ArrayList<Integer> producten) {
        this.datum = datum;
        this.klant = klant;
        this.producten = producten;
    }

    public ArrayList<Integer> getProducten() {
        return producten;
    }

    
}
