/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp;

import java.util.ArrayList;
import java.util.Random;
import javax.swing.JOptionPane;
import orders.Order;

/**
 *
 * @author Rienk
 */
//de grid bepaald de posities van de punten en de groottes van de grid die getekend wordt.
public class Grid {

    private ArrayList<Positie> punten;
    private int aantalPunten, grootte, maxPunten;
    private ArrayList<Integer> plekken;

    public Grid(Order order) {
        punten = new ArrayList<Positie>();
        grootte = 5;
        for (int i = 0; i < order.getProducten().size(); i++) {
            int plek = order.getProducten().get(i);
            int x = plek % grootte;
            int teller = 0;
            while (plek - grootte >= 0) {
                teller++;
                plek -= grootte;
            }
            int y = teller;
            x = 410 / grootte * x - 5;
            y = 400 / grootte * y - 5;
            punten.add(new Positie(x, y));
        }
    }

    public Grid(int aantalPunten, int grootte) {
        this.grootte = grootte;
        //controle voor het maximale punten in een grid.
        this.maxPunten = (int) Math.pow(grootte, 2);
        if (aantalPunten < 0) {
            JOptionPane.showMessageDialog(null, "Voer een positief getal in", "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
        if (aantalPunten <= maxPunten) {
            this.aantalPunten = aantalPunten;
        } else {
            JOptionPane.showMessageDialog(null, "Het aantal punten overschijdt het maximum voor de gridgrootte", "Error",
                    JOptionPane.ERROR_MESSAGE);
            this.aantalPunten = 0;
        }

        punten = new ArrayList<Positie>();
        plekken = new ArrayList<>();
        //toekennen van willekeurige posities voor in de grid.
        for (int i = 0; i < aantalPunten; i++) {
            Random rn = new Random();
            int plek = rn.nextInt(maxPunten);
            //voeg een punt toe als deze niet bestaat
            if (plekken.contains(plek)) {
                i--;
            } else {
                //X en Y waardes worden toegekent aan een positie.
                plekken.add(plek);
                int x = plek % grootte;
                int teller = 0;
                while (plek - grootte >= 0) {
                    teller++;
                    plek -= grootte;
                }
                int y = teller;
                x = 410 / grootte * x - 5;
                y = 400 / grootte * y - 5;
                punten.add(new Positie(x, y));
            }

        }
    }

    // kloon een bestaande arraylist met posities.
    public Grid(ArrayList<Positie> punten) {
        this.punten = new ArrayList<Positie>();
        this.punten = (ArrayList<Positie>) punten.clone();
        this.aantalPunten = this.punten.size();
    }

    public ArrayList<Positie> getPunten() {
        return punten;
    }

    //arraylist met posities vervangen door een nieuwe.
    public void setPunten(ArrayList<Positie> nieuwePunten) {
        ArrayList<Positie> temp = (ArrayList<Positie>) nieuwePunten.clone();
        punten.clear();
        for (int i = 0; i < temp.size(); i++) {
            punten.add(i, temp.get(i));
        }
    }

    public int getAantalPunten() {
        return punten.size();
    }

    public int getGrootte() {
        return grootte;
    }

    public void setGrootte(int grootte) {
        this.grootte = grootte;
    }
}
