/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp;

import tsp.algoritmes.TwoOptPanel;
import tsp.algoritmes.NearestNeighbourPanel;
import tsp.algoritmes.BruteForcePanel;
import java.awt.Dimension;
import javax.swing.JPanel;
import tsp.algoritmes.TwoNeighboursPanel;

/**
 *
 * @author Lars
 */
public class ContainerPanel extends JPanel {
    //initializeren variabelen
    private BruteForcePanel bruteForce;
    private NearestNeighbourPanel nearestNeighbour;
    private TwoOptPanel twoOpt;
    private TwoNeighboursPanel twoNeighbours;
    private Grid grid;

    public ContainerPanel(Grid grid) {
        this.grid = grid;
        setPreferredSize(new Dimension(900, 900));
        //Toevoegen panels van de algoritmes.
        bruteForce = new BruteForcePanel(grid);
        add(bruteForce);
        nearestNeighbour = new NearestNeighbourPanel(grid);
        add(nearestNeighbour);
        twoOpt = new TwoOptPanel(grid);
        add(twoOpt);
        twoNeighbours = new TwoNeighboursPanel(grid);
        add(twoNeighbours);
        
    }
    //functies om de algoritmes te starten en te stoppen.
    public void aanZetten() {
        bruteForce.aanZetten();
        nearestNeighbour.aanZetten();
        twoOpt.aanZetten();
        twoNeighbours.aanZetten();
    }

    public void uitZetten() {
        bruteForce.uitZetten();
        nearestNeighbour.uitZetten();
        twoOpt.uitZetten();
        twoNeighbours.uitZetten();
    }
    //functie om de huidige panels leeg te maken.
    public void leegMaken() {
        bruteForce.leegMaken();
        nearestNeighbour.leegMaken();
        twoOpt.leegMaken();
        twoNeighbours.leegMaken();
    }
    

}
