/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Rienk
 */
public class KiesPunten extends JDialog implements ActionListener {

    private JLabel jlGridGrootte;
    private JLabel jlAantalPunten;
    private JTextField jtfGridGrootte;
    private JTextField jtfAantalPunten;
    private JButton jbOk;
    private JButton jbAnnuleer;
    private int gridGrootte;
    private int aantalPunten;
    private boolean isOk = false;

    public KiesPunten(JFrame frame) {
        super(frame, true);
        setPreferredSize(new Dimension(200, 200));
        setLayout(new FlowLayout());

        jlGridGrootte = new JLabel("Grid grootte:");
        jtfGridGrootte = new JTextField(5);
        jlAantalPunten = new JLabel("Aantal punten:");
        jtfAantalPunten = new JTextField(5);
        jbOk = new JButton("Ok");
        jbAnnuleer = new JButton("Annuleer");
        jbOk.addActionListener(this);
        jbAnnuleer.addActionListener(this);

        add(jlGridGrootte);
        add(jtfGridGrootte);
        add(jlAantalPunten);
        add(jtfAantalPunten);
        add(jbOk);
        add(jbAnnuleer);

    }

    public int getGridGrootte() {
        return gridGrootte;
    }

    public int getAantalPunten() {
        return aantalPunten;
    }

    public boolean getIsOk() {
        return isOk;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == jbAnnuleer) {
            isOk = false;
        }
        //Wanneer OK is ingedrukt haal waardes op.
        if (e.getSource() == jbOk) {
            try {
                aantalPunten = Integer.parseInt(jtfAantalPunten.getText());
                gridGrootte = Integer.parseInt(jtfGridGrootte.getText());
                isOk = true;
            } catch (NumberFormatException nfe) {
                JOptionPane.showMessageDialog(null, "Voer getallen in!", "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
        dispose();
    }

}
