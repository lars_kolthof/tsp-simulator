/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp;

/**
 *
 * @author Rienk
 */
public class Positie {

    private int x;
    private int y;
    private static int nr;
    private int id;
    
    public Positie(int x, int y) {
        this.x = x;
        this.y = y;
        nr = nr + 1;
        id = nr;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
    //berekening van afstand tussen twee punten.
    public double afstand(Positie p) {
        double d = Math.sqrt(Math.pow(this.x-p.x, 2)+Math.pow(this.y-p.y, 2));
        return d;
        
    }
}
