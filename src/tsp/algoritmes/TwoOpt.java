/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp.algoritmes;

import java.awt.geom.Line2D;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import javax.swing.JLabel;
import tsp.Grid;
import tsp.Positie;

/**
 *
 * @author Lars
 */
public class TwoOpt extends Algoritme {

    private Grid grid;
    private Grid besteGrid;
    private double recordAfstand, afstand;
    private boolean klaar;
    private JLabel naam;
    private boolean w;
    private long start, done;

    public TwoOpt(Grid grid) {
        besteGrid = new Grid(0,10);
        this.grid = grid;
        klaar = false;
        this.randomRoute();
        naam = new JLabel("Two Opt");
        w = true;
        afstand = berekenAfstand(grid);
    }

    @Override
    public void berekenVolgorde() {
        afstand = berekenAfstand(besteGrid);
        if (w == true) {
            w = false;
            for (int i = 0; i < besteGrid.getPunten().size() - 1; i++) {
                for (int j = i + 1; j < besteGrid.getPunten().size() - 1; j++) {
//            bij elk punt i word gekeken waar de lijn naartoe gaat. hiervan word de x en y opgehaald. dit zijn de eerste 4 punten van line2D
//            voor de volgende 4 punten zoekt een loop naar coordinaten van alle lijnen die nog niet berekend zijn.
//            als er een kruizing is worden de punten gewisseld
                    if (Line2D.linesIntersect(besteGrid.getPunten().get(i).getX(), besteGrid.getPunten().get(i).getY(), besteGrid.getPunten().get(i + 1).getX(), besteGrid.getPunten().get(i + 1).getY(), besteGrid.getPunten().get(j).getX(), besteGrid.getPunten().get(j).getY(), besteGrid.getPunten().get(j + 1).getX(), besteGrid.getPunten().get(j + 1).getY())) {
                        if (i + 1 != j && j + 1 != i) {
                            Collections.swap(besteGrid.getPunten(), i + 1, j);
                            w = true;
                        }
                    }
                }
            }
            recordAfstand = berekenAfstand(besteGrid);

        }
        if (afstand == recordAfstand) {
            klaar = true;
        }

    }

    @Override
    public int berekenMogelijkheden() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    //bereken de totale afstand.
    @Override
    public double berekenAfstand(Grid grid) {
        double som = 0;
        for (int i = 0; i < grid.getPunten().size(); i++) {
            if (i + 1 < grid.getPunten().size()) {
                double d = Math.sqrt((grid.getPunten().get(i).getX() - grid.getPunten().get(i + 1).getX()) * (grid.getPunten().get(i).getX() - grid.getPunten().get(i + 1).getX()) + (grid.getPunten().get(i).getY() - grid.getPunten().get(i + 1).getY()) * (grid.getPunten().get(i).getY() - grid.getPunten().get(i + 1).getY()));
                som += d;
            }
        }
        return som;
    }

    //Maak een random route
    private ArrayList randomRoute() {
        Random rand = new Random();
        ArrayList<Positie> route = new ArrayList<>();
        while (besteGrid.getPunten().size() != grid.getPunten().size()) {
            //Voeg random punt toe in besteGrid als het punt nog niet bestaat.
            int rnd = rand.nextInt(((grid.getAantalPunten() - 1) - 0) + 1) + 0;
            if (!besteGrid.getPunten().contains(grid.getPunten().get(rnd))) {
                besteGrid.getPunten().add(grid.getPunten().get(rnd));
            }
        }
        return route;
    }

    public double getRecordAfstand() {
        return recordAfstand;
    }

    public boolean isKlaar() {
        return klaar;
    }

    public Grid getGrid() {
        return grid;
    }

    public Grid getBesteGrid() {
        return besteGrid;
    }

    public long getStart() {
        return start;
    }

    public long getDone() {
        return done;
    }

    public void setRecordAfstand(double recordAfstand) {
        this.recordAfstand = recordAfstand;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public void setDone(long done) {
        this.done = done;
    }

    public long getTijd() {
        return this.done - this.start;
    }
}
