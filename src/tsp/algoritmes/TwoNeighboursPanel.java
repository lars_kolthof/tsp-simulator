/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp.algoritmes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import static java.lang.System.currentTimeMillis;
import javax.swing.JLabel;
import javax.swing.JPanel;
import tsp.Grid;

/**
 *
 * @author Rienk
 */
public class TwoNeighboursPanel extends JPanel {

    private NearestNeighbour nn;
    private TwoOpt to;
    private Grid grid;
    private Grid besteGrid;
    private boolean optlaad, aan;
    private JLabel label;
    private double afstand;
    private int breedte = 410;
    private int hoogte = 400;

    public TwoNeighboursPanel(Grid grid) {
        setPreferredSize(new Dimension(breedte, hoogte));
        nn = new NearestNeighbour(grid);
        to = new TwoOpt(new Grid(0,10));
        this.grid = grid;
        this.besteGrid = new Grid(0,10);
        afstand = 0;
        optlaad = false;
        aan = false;
        label = new JLabel("Two Neighbours");
        label.setForeground(Color.blue);
        label.setFont(new Font("Arial", Font.PLAIN, 20));
        setBackground(Color.lightGray);
        add(label);
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g.setColor(Color.black);
        g.setFont(new Font("Arial", Font.BOLD, 16));
        // teken het grid
        for(int i=0;i<grid.getGrootte();i++){
            for(int j=0;j<grid.getGrootte();j++){
                g.drawRect(breedte/grid.getGrootte()*i, hoogte/grid.getGrootte()*j, breedte/grid.getGrootte(), hoogte/grid.getGrootte());
            }
        }
        //teken de punten en de bijbehorende nummers
        for (int i = 0; i < grid.getPunten().size(); i++) {
            g.fillOval(grid.getPunten().get(i).getX() + breedte / grid.getGrootte() / 2, grid.getPunten().get(i).getY() + hoogte / grid.getGrootte() / 2, 10, 10);
            g.drawString(Integer.toString(i + 1), grid.getPunten().get(i).getX() + 11 + breedte / grid.getGrootte() / 2, grid.getPunten().get(i).getY() + 11 + hoogte / grid.getGrootte() / 2);
        }
        //Kijk welke afstand er opgehaald moet worden
        if (optlaad) {
            afstand = to.getRecordAfstand();
        } else {
            afstand = nn.getRecordAfstand();
        }
        g.drawString(Double.toString(afstand), 350, 350);
        g.setColor(Color.red);
        //teken de beste optie in het rood
        for (int i = 0; i < besteGrid.getPunten().size(); i++) {
            if (i + 1 < besteGrid.getPunten().size()) {
                g2.setStroke(new BasicStroke(3));
                g2.drawLine(besteGrid.getPunten().get(i).getX() + 5 + breedte / grid.getGrootte() / 2, besteGrid.getPunten().get(i).getY() + 5 + hoogte / grid.getGrootte() / 2, besteGrid.getPunten().get(i + 1).getX() + 5 + breedte / grid.getGrootte() / 2, besteGrid.getPunten().get(i + 1).getY() + 5 + hoogte / grid.getGrootte() / 2);
            }
        }
        if (aan) {

            if (to.getStart() == 0) {
                to.setStart(currentTimeMillis());
            }
            // eerst nearest neighbour uitvoeren en daarna two opt
            if (!nn.isKlaar()) {
                nn.berekenVolgorde();
                grid = nn.getGrid();
                besteGrid = nn.getBesteGrid();
                //recursief uitvoeren totdat het algoritme klaar is
                repaint();
            } else {
                if (!optlaad) {
                    to.getGrid().setPunten(besteGrid.getPunten());
                    to.getGrid().setGrootte(nn.getGrid().getGrootte());
                    to.getBesteGrid().setPunten(besteGrid.getPunten());
                    to.getBesteGrid().setGrootte(nn.getGrid().getGrootte());
                    optlaad = true;
                }

                if (!to.isKlaar()) {
                    to.berekenVolgorde();
                    grid = to.getGrid();
                    besteGrid = to.getBesteGrid();
                    //recursief uitvoeren totdat het algoritme klaar is
                    repaint();
                }
            }
        }
        if (to.getStart() != 0) {
            //tijd bijhouden
            if (!to.isKlaar()) {
                to.setDone(currentTimeMillis());
            }
            g.drawString(Double.toString((double) (to.getTijd()) / 1000), 10, 350);
        } else {
            g.drawString("0.0", 10, 350);
        }

    }
//algoritme aan of uitzetten
    public void aanZetten() {
        aan = true;
        repaint();
    }

    public void uitZetten() {
        aan = false;
        repaint();
    }
//Alle gegevens leeg maken
    public void leegMaken() {
        grid.getPunten().clear();
        besteGrid.getPunten().clear();
        nn.setPercentage(0);
        nn.setRecordAfstand(0);
        nn.setDone(0);
        nn.setStart(0);
        to.setRecordAfstand(0);
        to.setDone(0);
        to.setStart(0);
        this.uitZetten();
    }

}
