/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp.algoritmes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import static java.lang.System.currentTimeMillis;
import javax.swing.JPanel;
import javax.swing.JLabel;
import tsp.Grid;

/**
 *
 * @author Rienk
 */
public class TwoOptPanel extends JPanel {

    private TwoOpt to;
    private Grid grid;
    private Grid besteGrid;
    private JLabel label;

    private int breedte = 410;
    private int hoogte = 400;
    private boolean aan;
    public TwoOptPanel(Grid grid){
        to = new TwoOpt(grid);
        besteGrid = new Grid(grid.getPunten());
        this.grid = grid;
        label = new JLabel("Two Opt");
        label.setForeground(Color.blue);
        label.setFont(new Font("Arial", Font.PLAIN, 20));
        add(label);
        setBackground(Color.lightGray);
        setPreferredSize(new Dimension(410, 400));
        aan = false;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g.setFont(new Font("Arial", Font.BOLD, 16));
        besteGrid = to.getBesteGrid();

        g.setColor(Color.black);
        // teken het grid
        for(int i=0;i<grid.getGrootte();i++){
            for(int j=0;j<grid.getGrootte();j++){
                g.drawRect(breedte/grid.getGrootte()*i, hoogte/grid.getGrootte()*j, breedte/grid.getGrootte(), hoogte/grid.getGrootte());
            }
            g.drawString(Double.toString(to.getRecordAfstand()), 350, 350);       

        }
        // teken de punten met het bijbehorende nummer 
        for (int i = 0; i < besteGrid.getPunten().size(); i++) {
            g.fillOval(besteGrid.getPunten().get(i).getX()+breedte/grid.getGrootte()/2, besteGrid.getPunten().get(i).getY()+hoogte/grid.getGrootte()/2, 10, 10);
            g.drawString(Integer.toString(i + 1), besteGrid.getPunten().get(i).getX() + 11+breedte/grid.getGrootte()/2, besteGrid.getPunten().get(i).getY() + 11+hoogte/grid.getGrootte()/2);
        }

        g.setColor(Color.red);
        // teken de beste route in het rood
        for (int i = 0; i < besteGrid.getPunten().size(); i++) {
            if (i + 1 < besteGrid.getPunten().size()) {
                g2.setStroke(new BasicStroke(3));
                g2.drawLine(besteGrid.getPunten().get(i).getX() + 5+breedte/grid.getGrootte()/2, besteGrid.getPunten().get(i).getY() + 5+hoogte/grid.getGrootte()/2, besteGrid.getPunten().get(i + 1).getX() + 5+breedte/grid.getGrootte()/2, besteGrid.getPunten().get(i + 1).getY() + 5+hoogte/grid.getGrootte()/2);
            }
        }        
        if(aan){
            if(to.getStart() == 0){
                to.setStart(currentTimeMillis());
            }
            to.berekenVolgorde();     
        }
         //recursief uitvoeren totdat het algoritme klaar is
        if(!(to.isKlaar())){
            repaint();
        }
        if(to.getStart() != 0){
            //tijd bijhouden
            if(!to.isKlaar()){
                to.setDone(currentTimeMillis());
            }
            g.drawString(Double.toString((double)(to.getTijd())/1000), 10, 350);
        }else{
            g.drawString("0.0",10,350);
        }
    }
    //algoritme aan of uitzetten
    public void aanZetten(){
        aan = true;
        repaint();
    }
    public void uitZetten(){
        aan = false;
        repaint();
    }
    //Alle gegevens leeg maken
    public void leegMaken() {
        grid.getPunten().clear();
        besteGrid.getPunten().clear();
        to.setRecordAfstand(0);
        to.setDone(0);
        to.setStart(0);
        this.uitZetten();
    }
    


}
