/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp.algoritmes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import static java.lang.System.currentTimeMillis;
import javax.swing.JLabel;
import javax.swing.JPanel;
import tsp.Grid;

/**
 *
 * @author Rienk
 */
public class NearestNeighbourPanel extends JPanel {

    private Grid grid;
    private Grid besteGrid;
    private JLabel label;
    private NearestNeighbour nn;
    private int breedte = 410;
    private int hoogte = 400;
    private boolean aan;

    public NearestNeighbourPanel(Grid grid) {
        setPreferredSize(new Dimension(breedte, hoogte));
        this.grid = grid;
        this.besteGrid = new Grid(grid.getPunten());
        nn = new NearestNeighbour(grid);
        label = new JLabel("Nearest Neighbour");
        label.setForeground(Color.blue);
        label.setFont(new Font("Arial", Font.PLAIN, 20));
        add(label);
        setBackground(Color.lightGray);
        aan = false;
    }

    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g.setColor(Color.black);
        g.setFont(new Font("Arial", Font.BOLD, 16));
        //teken grid
        for(int i=0;i<grid.getGrootte();i++){
            for(int j=0;j<grid.getGrootte();j++){
                g.drawRect(breedte/grid.getGrootte()*i, hoogte/grid.getGrootte()*j, breedte/grid.getGrootte(), hoogte/grid.getGrootte());
            }
        }
        //schrijf het percentage
        g.drawString(Double.toString(nn.getPercentage()) + " % compleet", 10, 15);
        //haal de grids op uit het algoritme
        grid = nn.getGrid();
        besteGrid = nn.getBesteGrid();
          //teken de punten en het bijbehorend nummer
        for (int i = 0; i < grid.getPunten().size(); i++) {
            g.fillOval(grid.getPunten().get(i).getX() + breedte / grid.getGrootte() / 2, grid.getPunten().get(i).getY() + hoogte / grid.getGrootte() / 2, 10, 10);
            g.drawString(Integer.toString(i + 1), grid.getPunten().get(i).getX() + 11 + breedte / grid.getGrootte() / 2, grid.getPunten().get(i).getY() + +hoogte / grid.getGrootte() / 2);

            g.drawString(Double.toString(nn.getRecordAfstand()), 350, 350);
        }
        g.setColor(Color.red);
        //teken de beste optie in het rood
        for (int i = 0; i < besteGrid.getPunten().size(); i++) {
            if (i + 1 < besteGrid.getPunten().size()) {
                g2.setStroke(new BasicStroke(3));
                g2.drawLine(besteGrid.getPunten().get(i).getX() + 5 + breedte / grid.getGrootte() / 2, besteGrid.getPunten().get(i).getY() + 5 + hoogte / grid.getGrootte() / 2, besteGrid.getPunten().get(i + 1).getX() + 5 + breedte / grid.getGrootte() / 2, besteGrid.getPunten().get(i + 1).getY() + 5 + hoogte / grid.getGrootte() / 2);
            }
        }
        //Starten met het berekenen van het algoritme

        if (aan) {
            if (!nn.isKlaar()) {
                if (nn.getStart() == 0) {
                    nn.setStart(currentTimeMillis());
                }
                nn.berekenVolgorde();
                //recursief uitvoeren totdat het algoritme klaar is
                repaint();

            }
        } else {
            if (nn.getDone() == 0) {
                g.drawString("0.0", 10, 350);
            }
        }
        if (nn.getStart() != 0) {
            if (!nn.isKlaar()) {
                nn.setDone(currentTimeMillis());
            }
            g.drawString(Double.toString((double) (nn.getTijd()) / 1000), 10, 350);
        }

    }
    //algoritme aan of uitzetten
    public void aanZetten(){
        if(!aan){
            aan = true;
            repaint();
        }
    }
    public void uitZetten(){
        if(aan){
            aan = false;
            repaint();
        }

    }
//Alle gegevens leeg maken
    public void leegMaken() {
        grid.getPunten().clear();
        besteGrid.getPunten().clear();
        nn.setPercentage(0);
        nn.setRecordAfstand(0);
        nn.setDone(0);
        nn.setStart(0);
        this.uitZetten();
    }
    
}
