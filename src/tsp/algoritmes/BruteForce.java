/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp.algoritmes;

import java.util.ArrayList;
import java.util.Collections;
import tsp.Grid;
import tsp.Positie;

/**
 *
 * @author Rienk
 */
public class BruteForce extends Algoritme {

    private Grid grid;
    private Grid besteGrid;
    private double d;
    private double recordAfstand;
    private double percentage;
    private double teller = 1;
    private boolean klaar = false;
    private long done, stopTijd, stop, start;

    public BruteForce(Grid grid) {
        this.grid = grid;
        this.besteGrid = new Grid(grid.getPunten());
        punten = new ArrayList<>();
        for (int i = 0; i < grid.getPunten().size(); i++) {
            punten.add(grid.getPunten().get(i));
        }
        //maak een arraylist met getallen die gelijk zijn aan het aantal indexen van de posities.
        volgorde = new ArrayList<>();
        for (int i = 0; i < grid.getPunten().size(); i++) {
            volgorde.add(i);
        }

        d = berekenAfstand(grid);
        recordAfstand = d;
        aantalMogelijkheden = this.berekenMogelijkheden();
        //Loop tijd bijhouden inclusief de tijd tussen start en stop
        done = 0;
        stopTijd = 0;
        stop = 0;
        start = 0;
    }

    //Het brute force algoritme
    @Override
    public void berekenVolgorde() {
        if (teller < aantalMogelijkheden) {
            teller++;
        }
        percentage = (teller * 100) / aantalMogelijkheden;
        percentage = Math.round(percentage * 100);
        percentage = percentage / 100;
        int grootsteI = -1;
        //bereken de indexen om te wisselen.
        for (int i = 0; i < volgorde.size() - 1; i++) {
            if (volgorde.get(i) < volgorde.get(i + 1)) {
                grootsteI = i;
            }
        }

        if (grootsteI != -1) {
            int grootsteJ = -1;
            for (int j = 0; j < volgorde.size(); j++) {
                if (volgorde.get(j) > volgorde.get(grootsteI)) {
                    grootsteJ = j;
                }
            }
            //wissel de indexen.
            Collections.swap(volgorde, grootsteI, grootsteJ);
            //volgorde bewerken.
            volgorde = bewerkLijst(volgorde, grootsteI + 1);
            //posities wijzigen op basis van de volgorde.
            bewerkPunten(volgorde);

        } else {
            klaar = true;
        }
// loop om te bekijken hoe de volgorde gaat.
//        for(int i = 0 ; i< volgorde.size(); i++){
//            System.out.print(volgorde.get(i));
//        }
//        System.out.println();
    }

    public ArrayList<Integer> bewerkLijst(ArrayList<Integer> lijst, int i) {
        //splice deel van volgorde, draai dat deel om en voeg opnieuw toe.
        ArrayList<Integer> tmpLijst = new ArrayList<>();
        int g = lijst.size();
        for (int j = i; j < g; j++) {
            tmpLijst.add(lijst.get(j));
        }
        for (int j = g - 1; j >= i; j--) {
            lijst.remove(j);
        }
        Collections.reverse(tmpLijst);
        lijst.addAll(tmpLijst);
        return lijst;
    }
    //recursieve methode bereken faculteit voor het aantal mogelijkheden
    public int faculteit(int n) {
        if (n == 1) {
            return 1;
        } else {
            return n * faculteit(n - 1);
        }
    }
    //map de volgorde van de posities arraylist op basis van de volgorde
    public void bewerkPunten(ArrayList<Integer> volgorde) {
        Positie[] nieuweVolgorde = new Positie[volgorde.size()];
        for (int i = 0; i < volgorde.size(); i++) {
            int j = volgorde.get(i);
            nieuweVolgorde[j] = grid.getPunten().get(i);
        }
        this.setPunten(nieuweVolgorde);
    }

    public void setPunten(Positie[] nieuweVolgorde) {
        grid.getPunten().clear();
        for (int i = 0; i < nieuweVolgorde.length; i++) {
            grid.getPunten().add(i, nieuweVolgorde[i]);
        }
        d = berekenAfstand(grid);
        if (d < recordAfstand) {
            recordAfstand = d;
            besteGrid.getPunten().clear();
            for (int i = 0; i < nieuweVolgorde.length; i++) {
                besteGrid.getPunten().add(i, nieuweVolgorde[i]);
            }
        }
    }

    @Override
    public int berekenMogelijkheden() {
        return this.faculteit(grid.getAantalPunten());
    }

    public double getRecordAfstand() {
        return recordAfstand;
    }

    public double getPercentage() {
        return percentage;
    }

    public boolean isKlaar() {
        return klaar;
    }

    public Grid getGrid() {
        return grid;
    }

    public Grid getBesteGrid() {
        return besteGrid;
    }

    public void setRecordAfstand(double recordAfstand) {
        this.recordAfstand = recordAfstand;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public void setDone(long done) {
        this.done = done;
    }

    public long getStart() {
        return start;
    }

    public long getDone() {
        return done;
    }

    public long getTijd() {
            return this.done - this.start - this.stopTijd;
        
    }

    public long getStopTijd() {
        return stopTijd;
    }

    public void setStopTijd(long stopTijd) {
        this.stopTijd += stopTijd;
    }

    public long getStop() {
        return stop;
    }

    public void setStop(long stop) {
        this.stop = stop;
    }


}
