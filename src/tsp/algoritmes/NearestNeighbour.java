/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp.algoritmes;

import tsp.algoritmes.Algoritme;
import java.util.ArrayList;
import tsp.Grid;
import tsp.Positie;

/**
 *
 * @author Rienk
 */
public class NearestNeighbour extends Algoritme {

    private Grid grid;
    private Grid besteGrid;
    private double recordAfstand;
    private Positie startPositie;
    private int grootte, i, teller, aantalMogelijkheden;
    private ArrayList<Positie> loop;
    private Positie ont;
    private double percentage;
    private boolean klaar;
    private long start, done;

    public NearestNeighbour(Grid grid) {
        this.grid = grid;
        this.besteGrid = new Grid(0,10);
        grootte = grid.getPunten().size();
        i = 0;
        teller = 0;
        klaar = false;
        // loop gebruik je om overal langs te komen.
        loop = (ArrayList<Positie>) grid.getPunten().clone();
        // het eerste punt uit de array gebruiken als startpositie.
        startPositie = loop.get(0);
        besteGrid.getPunten().add(startPositie);
        // eerste punt verwijderen zodat deze niet weer wordt gebruikt.
        loop.remove(startPositie);

        aantalMogelijkheden = this.berekenMogelijkheden();
    }

    //het nearest neighbour algoritme.
    @Override
    public void berekenVolgorde() {
        double kortsteAfstand = 0;
        percentage = (teller * 100) / aantalMogelijkheden;
        percentage = Math.round(percentage * 100);
        percentage = percentage / 100;
        for (Positie np : loop) {
            //vergelijk het laatste punt in de besteGrid met de overgebleven posities 
            double afstand = Math.sqrt((besteGrid.getPunten().get(i).getX() - np.getX()) * (besteGrid.getPunten().get(i).getX() - np.getX()) + (besteGrid.getPunten().get(i).getY() - np.getY()) * (besteGrid.getPunten().get(i).getY() - np.getY()));
            if (kortsteAfstand == 0) {
                kortsteAfstand = afstand;
                ont = np;
            } else if (afstand < kortsteAfstand) {
                kortsteAfstand = afstand;
                ont = np;
            }
            teller++;
        }
        i++;
        //voeg het punt met de kortste afstand toe aan de beste array, en verwijder het uit de loop
        besteGrid.getPunten().add(ont);
        loop.remove(ont);
        if (!(besteGrid.getPunten().size() == grootte + 1)) {
            recordAfstand = berekenAfstand(besteGrid);
        } else {
            klaar = true;
        }
    }

    @Override
    // functie om het aantal mogelijkheden te berekenen
    public int berekenMogelijkheden() {
        int som = 0;
        for (int j = 1; j < grid.getPunten().size(); j++) {
            som += grid.getPunten().size() - j;
        }
        return som;
    }

    public Grid getGrid() {
        return grid;
    }

    public Grid getBesteGrid() {
        return besteGrid;
    }

    public double getRecordAfstand() {
        return recordAfstand;
    }

    public double getPercentage() {
        return percentage;
    }

    public boolean isKlaar() {
        return klaar;
    }

    public long getStart() {
        return start;
    }

    public long getDone() {
        return done;
    }

    public void setRecordAfstand(double recordAfstand) {
        this.recordAfstand = recordAfstand;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public void setStart(long start) {
        this.start = start;
    }

    public void setDone(long done) {
        this.done = done;
    }

        
    public long getTijd(){
        return this.done-this.start;
    }
}
