/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp.algoritmes;

import java.util.ArrayList;
import tsp.Grid;
import tsp.Positie;

/**
 *
 * @author Rienk
 */
public abstract class Algoritme{
    ArrayList<Positie> punten;
    ArrayList<Integer> volgorde;
    long tijd;
    long aantalMogelijkheden;
    
    public abstract void berekenVolgorde();
    
    public abstract int berekenMogelijkheden();
    
    public void setPunten(){
        
    } 
    //berekend de afstand tussen twee posities
    public double berekenAfstand(Grid grid) {
        double som = 0;
        for (int i = 0; i < grid.getPunten().size(); i++) {
            if (i + 1 < grid.getPunten().size()) {
                double d = Math.sqrt((grid.getPunten().get(i).getX() - grid.getPunten().get(i + 1).getX()) * (grid.getPunten().get(i).getX() - grid.getPunten().get(i + 1).getX()) + (grid.getPunten().get(i).getY() - grid.getPunten().get(i + 1).getY()) * (grid.getPunten().get(i).getY() - grid.getPunten().get(i + 1).getY()));
                som += d;
            }
        }
        return som;
    }
    
    public ArrayList<Positie> getPunten(){
        return this.punten;
    }
    
    public long getTijd(){
        return this.tijd;
    }
}
