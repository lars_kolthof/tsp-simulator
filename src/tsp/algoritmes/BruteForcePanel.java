/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp.algoritmes;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import static java.lang.System.currentTimeMillis;
import javax.swing.JLabel;
import javax.swing.JPanel;
import tsp.Grid;

/**
 *
 * @author Rienk
 */
public class BruteForcePanel extends JPanel {

    private BruteForce bf;
    private Grid grid;
    private Grid besteGrid;
    private JLabel label;
    private int breedte = 410;
    private int hoogte = 400;
    private boolean aan;

    public BruteForcePanel(Grid grid) {
        setPreferredSize(new Dimension(breedte, hoogte));
        bf = new BruteForce(grid);
        label = new JLabel("Brute Force");
        label.setForeground(Color.blue);
        label.setFont(new Font("Arial", Font.PLAIN, 20));
        this.grid = grid;
        this.besteGrid = grid;
        setBackground(Color.lightGray);
        add(label);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
               g.setFont(new Font("Arial", Font.BOLD, 16));

        for (int i = 0; i < grid.getGrootte(); i++) {
            for (int j = 0; j < grid.getGrootte(); j++) {
                //teken grid
                g.drawRect(breedte / grid.getGrootte() * i, hoogte / grid.getGrootte() * j, breedte / grid.getGrootte(), hoogte / grid.getGrootte());
            }
        }
        //haal de grids op uit het algoritme
        grid = bf.getGrid();
        besteGrid = bf.getBesteGrid();
        g.setColor(Color.black);
        //schrijf het percentage
        g.drawString(Double.toString(bf.getPercentage())+" % compleet", 10, 15);
        //teken de punten en het bijbehorend nummer
        for (int i = 0; i < grid.getPunten().size(); i++) {
            g.fillOval(grid.getPunten().get(i).getX() + breedte / grid.getGrootte() / 2, grid.getPunten().get(i).getY() + hoogte / grid.getGrootte() / 2, 10, 10);
            g.drawString(Integer.toString(i + 1), grid.getPunten().get(i).getX() + 11 + breedte / grid.getGrootte() / 2, grid.getPunten().get(i).getY() + 11 + hoogte / grid.getGrootte() / 2);
            if (aan) {
                if (i + 1 < grid.getPunten().size()) {
                    g.drawLine(grid.getPunten().get(i).getX() + 5 + breedte / grid.getGrootte() / 2, grid.getPunten().get(i).getY() + 5 + hoogte / grid.getGrootte() / 2, grid.getPunten().get(i + 1).getX() + 5 + breedte / grid.getGrootte() / 2, grid.getPunten().get(i + 1).getY() + 5 + hoogte / grid.getGrootte() / 2);
                }
            }
            g.drawString(Double.toString(bf.getRecordAfstand()), 350, 350);
        }

        g.setColor(Color.red);
        //teken beste optie in het rood
        for (int i = 0; i < besteGrid.getPunten().size(); i++) {
            if (i + 1 < besteGrid.getPunten().size()) {
                g2.setStroke(new BasicStroke(3));
                g2.drawLine(besteGrid.getPunten().get(i).getX() + 5 + breedte / grid.getGrootte() / 2, besteGrid.getPunten().get(i).getY() + 5 + hoogte / grid.getGrootte() / 2, besteGrid.getPunten().get(i + 1).getX() + 5 + breedte / grid.getGrootte() / 2, besteGrid.getPunten().get(i + 1).getY() + 5 + hoogte / grid.getGrootte() / 2);
            }
        }
        if (aan) {
            //Starten met het berekenen van het algoritme
            if (bf.getStart() == 0) {
                bf.setStart(currentTimeMillis());
            }
            bf.berekenVolgorde();
            //recursief uitvoeren totdat het algoritme klaar is
            if (!(bf.isKlaar())) {
                repaint();
            }
        }
        
        if (bf.getStart() != 0) {
            //tijd bijhouden
            if (!bf.isKlaar()) {
                bf.setDone(currentTimeMillis());
            }
            g.drawString(Double.toString((double) (bf.getTijd()) / 1000), 10, 350);
        } else {
            g.drawString("0.0", 10, 350);
        }
    }
    //algoritme aan of uitzetten
    public void aanZetten(){
        if(!aan){
            if(bf.getStop() == 0){
                bf.setStopTijd(0);
            }else{
                bf.setStopTijd(currentTimeMillis()-bf.getStop());
            }
        }
        aan = true;
        repaint();
    }
    public void uitZetten(){
        if(aan){
            bf.setStop(currentTimeMillis());
        }
        aan = false;
        repaint();
    }
    //Alle gegevens leeg maken
    public void leegMaken() {
        grid.getPunten().clear();
        besteGrid.getPunten().clear();
        bf.setPercentage(0);
        bf.setRecordAfstand(0);
        bf.setDone(0);
        bf.setStart(0);
        this.uitZetten();
    }

}
