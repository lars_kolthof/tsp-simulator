/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tsp;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import orders.Klant;
import orders.Order;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import tsp.algoritmes.BruteForcePanel;

/**
 *
 * @author Rienk
 */
public class Scherm extends JFrame implements ActionListener {

    //Variabelen initializeren
    private int hoogte = 900;
    private int breedte = 1100;
    private Grid grid;
    private ContainerPanel container;
    private JButton start, rand, upload, empty, stop;
    private boolean aan;
    private KiesPunten dialoogPunten;

    public Scherm() {
        setTitle("TSP-simulator");
        setSize(breedte, hoogte);
        setLayout(new FlowLayout());
        grid = new Grid(7, 10);

        container = new ContainerPanel(grid);
        add(container);
        //Maak verticale lijst met knoppen.
        Box box = Box.createVerticalBox();
        start = new JButton("START");
        rand = new JButton("Willekeurig");
        upload = new JButton("Voer pakbon in");
        empty = new JButton("Leegmaken");
        stop = new JButton("STOP");
        start.addActionListener(this);
        rand.addActionListener(this);
        upload.addActionListener(this);
        empty.addActionListener(this);
        stop.addActionListener(this);
        //Knoppen toevoegen en uitlijnen
        box.add(start);
        box.add(Box.createVerticalStrut(50));
        box.add(rand);
        box.add(Box.createVerticalStrut(50));
        box.add(upload);
        box.add(Box.createVerticalStrut(50));
        box.add(empty);
        box.add(Box.createVerticalStrut(50));
        box.add(stop);
        add(box);
        //dialogbox voor het bepalen van de grootte van de grid en het aantal punten.
        dialoogPunten = new KiesPunten(this);
        dialoogPunten.setVisible(false);
        setVisible(true);
        //aan wordt gebruikt om de algoritmes te laten lopen.
        aan = false;

    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == start) {
            container.aanZetten();
        }
        if (e.getSource() == stop) {
            container.uitZetten();
        }
        if (e.getSource() == empty) {
            container.leegMaken();
        }
        //Maak nieuwe grid aan op basis van de ingevulde waardes uit het dialoog.
        if (e.getSource() == rand) {
            dialoogPunten.setSize(new Dimension(200,150));
            dialoogPunten.setVisible(true);
            if (dialoogPunten.getIsOk()) {
                grid = new Grid(dialoogPunten.getAantalPunten(), dialoogPunten.getGridGrootte());
                remove(container);
                //Maak een nieuw ContainerPanel.
                container = new ContainerPanel(grid);
                this.add(container, 0);
                revalidate();
            }

        }
        if (e.getSource() == upload) {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
            int result = fileChooser.showOpenDialog(this);
            File selectedFile = null;
            if (result == JFileChooser.APPROVE_OPTION) {
                selectedFile = fileChooser.getSelectedFile();
                System.out.println("Selected file: " + selectedFile.getAbsolutePath());

                DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
                try {
                    DocumentBuilder builder = documentFactory.newDocumentBuilder();
                    Document doc = builder.parse(selectedFile.getAbsolutePath());
                    NodeList rootNodes = doc.getElementsByTagName("bestelling");
                    Node rootNode = rootNodes.item(0);
                    Element rootElement = (Element) rootNode;
                    Node datumNode = rootElement.getElementsByTagName("datum").item(0);
                    Element datum = (Element) datumNode;
                    NodeList klant = rootElement.getElementsByTagName("klant");
                    Node klantNode = klant.item(0);
                    Element klantElement = (Element) klantNode;
                    Node voornaamNode = klantElement.getElementsByTagName("voornaam").item(0);
                    Element voornaam = (Element) voornaamNode;
                    Node achternaamNode = klantElement.getElementsByTagName("achternaam").item(0);
                    Element achternaam = (Element) achternaamNode;
                    Node adresNode = klantElement.getElementsByTagName("adres").item(0);
                    Element adres = (Element) adresNode;
                    Node plaatsNode = klantElement.getElementsByTagName("plaats").item(0);
                    Element plaats = (Element) plaatsNode;
                    Node postcodeNode = klantElement.getElementsByTagName("postcode").item(0);
                    Element postcode = (Element) postcodeNode;
                    NodeList productenNodes = rootElement.getElementsByTagName("artikelnr");
                    ArrayList<Integer> producten = new ArrayList<>();
                    for (int i = 0; i < productenNodes.getLength(); i++) {
                        Node artikelnr = productenNodes.item(i);
                        Element artikel = (Element) artikelnr;
                        producten.add(Integer.parseInt(artikel.getTextContent()));
                    }
                    Order order = new Order(datum.getTextContent(), new Klant(voornaam.getTextContent(), achternaam.getTextContent(), adres.getTextContent(), postcode.getTextContent(), plaats.getTextContent()), producten);
                    grid = new Grid(order);
                    remove(container);
                    //Maak een nieuw ContainerPanel.
                    container = new ContainerPanel(grid);
                    this.add(container, 0);
                    revalidate();
                } catch (ParserConfigurationException ex) {
                    JOptionPane.showMessageDialog(null, "Parse error!!", "Error",
                            JOptionPane.ERROR_MESSAGE);
                } catch (SAXException ex) {
                    JOptionPane.showMessageDialog(null, "Selecteer een xml-bestand", "Error",
                            JOptionPane.ERROR_MESSAGE);
                } catch (IOException ex) {
                    JOptionPane.showMessageDialog(null, "Er is een I/O error opgetreden :(", "Error",
                            JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

}
